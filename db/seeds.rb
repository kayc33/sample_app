User.create!(name:  "kelechi",
             email: "kc@gmail.com",
             password:              "12345678",
             password_confirmation: "12345678",
             phone_number: "028149444456",
             admin: true,
             paid: true,
             merge: 0,
             activated: true,
             activated_at: Time.zone.now)

User.create!(name:  "kelechi",
             email: "kc1@gmail.com",
             password:              "12345678",
             password_confirmation: "12345678",
             phone_number: "08149444457",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)



9.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
              
end
