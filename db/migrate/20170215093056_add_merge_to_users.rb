class AddMergeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :merge, :integer, default: 0
  end
end
