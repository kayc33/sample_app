class AddIndexToUsersAccountNumber < ActiveRecord::Migration[5.0]
  def change
    add_index :users, :account_number, unique: true
  end
end
