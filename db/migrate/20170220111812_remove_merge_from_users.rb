class RemoveMergeFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :merge, :integer
  end
end
